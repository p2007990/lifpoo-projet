package modele.plateau;

import modele.deplacements.Direction;
import java.awt.Point;

public class Colonne extends EntiteDynamique {

    private ColonneSegment[] listeSegments;
    private int taille;
    int yAcroche;
    public Colonne(Jeu _jeu, int tailleColonne, int xHaut, int yHaut, int _yAcroche) {
        super(_jeu);
        yAcroche = _yAcroche;
        listeSegments = new ColonneSegment[tailleColonne];
        taille  = tailleColonne;
        for (int i = 0; i < tailleColonne; i++)
        {
            Entite colSeg = new ColonneSegment(_jeu, this);
            listeSegments[i] = (ColonneSegment) colSeg;
            _jeu.addEntite(colSeg, xHaut, yHaut + i);
        }
    }

    @Override
    //TODO faire en sorte que les pillier detruise les entiter sur le chemin
    public boolean avancerDirectionChoisie(Direction d) {
        boolean ret = true;
        Point p1 = jeu.getPosition(listeSegments[0]);
        if (d == Direction.bas && p1.y == yAcroche)
            return false;
        Point p2 = jeu.getPosition(listeSegments[taille -1]);
        if (d == Direction.haut && p2.y == yAcroche)
            return false;

        if (d == Direction.haut)
        {
            for (int i = 0; i < taille; i++) {
                ret = ret && listeSegments[i].avancerDirectionChoisie(d);
            }
        }
        else
        {
            for (int i = taille -1 ; i >= 0; i--) {
                ret = ret && listeSegments[i].avancerDirectionChoisie(d);
            }
        }
        return ret;
    }

    @Override
    public Entite regarderDansLaDirection(Direction d) {
        if (d == Direction.droite || d == Direction.gauche)
            return null;
        if (d == Direction.haut)
            return jeu.regarderDansLaDirection(listeSegments[0], Direction.haut);
        else
            return jeu.regarderDansLaDirection(listeSegments[taille -1], Direction.bas);
    }

    public boolean peutEtreEcrase() { return false; }
    public boolean peutServirDeSupport() { return false;}
    public boolean peutPermettreDeMonterDescendre() { return false; };
}
