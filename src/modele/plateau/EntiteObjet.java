package modele.plateau;

public abstract class EntiteObjet extends Entite{
    public EntiteObjet(Jeu _jeu) {
        super(_jeu);
    }
    public boolean peutEtreEcrase() { return true; }
    public boolean peutServirDeSupport() { return false; }
    public boolean peutPermettreDeMonterDescendre() { return false; }
}