/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.plateau;

import modele.deplacements.*;

import java.awt.Point;
import java.util.HashMap;

/** Actuellement, cette classe gère les postions
 * (ajouter conditions de victoire, chargement du plateau, etc.)
 */
public class Jeu {

    private Heros hector;
    private Bot bot;
    private Colonne colonne;
    private Mur mur;
    private Corde corde;
    private Bombe bombe;
    private Radis radis;
    private int nbBombes=0; //attribut permettant de garder en mémoire le nombre de bombes ramassées
    private int nbRadis=0; //attribut permettant de garder en mémoire le nombre de radis ramassées
    private int vie = 5;  //nombre de vie restant à hector
    public static final int SIZE_X = 25;
    public static final int SIZE_Y = 10;

    // compteur de déplacements horizontal et vertical (1 max par défaut, à chaque pas de temps)
    private HashMap<Entite, Integer> cmptDeplH = new HashMap<Entite, Integer>();
    private HashMap<Entite, Integer> cmptDeplV = new HashMap<Entite, Integer>();


    private HashMap<Entite, Point> map = new  HashMap<Entite, Point>(); // permet de récupérer la position d'une entité à partir de sa référence
    private Entite[][] grilleEntites = new Entite[SIZE_X][SIZE_Y]; // permet de récupérer une entité à partir de ses coordonnées

    private Ordonnanceur ordonnanceur = new Ordonnanceur(this);

    public Jeu() {
        initialisationDesEntites();
    }

    public void resetCmptDepl() {
        cmptDeplH.clear();
        cmptDeplV.clear();
    }

    public void start(long _pause) {
        ordonnanceur.start(_pause);
    }
    
    public Entite[][] getGrille() {
        return grilleEntites;
    }

    public Point getPosition(Entite e)
    {
        return map.get(e);
    }

    
    public Heros getHector() {
        return hector;
    }

    private void createBlock(int x, int y, int z, Entite e, int choix) // (debut, fin, colonne ou ligne, Entite, choix (0 si ligne, 1 si colonne))
    {
        //Construit des murs par defauly si on choisie de construire en ligne
        if(choix==0)
        {
            for (int j = x; j <= y; j++) {
                addEntite(new Mur(this), j, z);
            }
        }else {
            //Construit des murs/colonnes/cordes verticalement en fonction du type d'entite
            for (int i = x; i <= y; i++)
            {
                if(e == mur) {
                    addEntite(new Mur(this), z, i);
                }
                else if (e == corde) {
                    addEntite(new Corde(this), z, i);
                }
            }
        }
    }


    private void createColonne(int x, int y, int taille, int yAccroche)
    {
        modele.deplacements.Colonne depCol = new modele.deplacements.Colonne();
        ordonnanceur.add(depCol);
        Colonne c = new Colonne(this, taille, x, y, yAccroche);
        depCol.addEntiteDynamique(c);
    }
    private void createBot(int x, int y)
    {
        modele.deplacements.Bot depBot = new modele.deplacements.Bot();
        Bot b = new Bot(this);
        depBot.addEntiteDynamique(b);
        ordonnanceur.add(depBot);

        addEntite(b, x, y);
    }
    private void Level1()
    {
        //murs extérieurs horizontaux
        createBlock(0, 19, 0, mur, 0);
        createBlock(0, 5, 9, mur, 0);
        createBlock(8, 19, 9, mur, 0);

        //murs extérieurs verticaux
        for (int y = 1; y <= 8; y++) {
            addEntite(new Mur(this), 0, y);
            addEntite(new Mur(this), 19, y);
        }

        //MURS
        createBlock(2, 3, 3, mur, 0);
        createBlock(4, 5, 8, mur, 0);
        createBlock(10, 13, 3, mur, 0);
        createBlock(8, 11, 7, mur, 0);
        createBlock(4, 5, 13, mur, 1);
        createBlock(16, 17, 5, mur, 0);
        addEntite(mur,6,3);
        addEntite(mur,8,3);
        addEntite(mur,8,8);
        addEntite(mur,9,6);
        addEntite(mur,11,6);

        //COLONNES
        createColonne(4, 3, 3, 3);
        createColonne(5, 3, 3, 3);
        createColonne(7, 3, 3, 3);

//        createBlock(1, 3, 4, colonne, 1);
//        createBlock(1, 3, 5, colonne, 1);
//        createBlock(5, 7, 6, colonne, 1);
//        createBlock(4, 6, 7, colonne, 1);
//        createBlock(1, 3, 9, colonne, 1);
//        createBlock(3, 5, 14, colonne, 1);
//        createBlock(3, 5, 15, colonne, 1);

        //CORDES
        createBlock(1, 4, 1, corde, 1);
        createBlock(4, 8, 2, corde, 1);
        createBlock(4, 6, 8, corde, 1);
        createBlock(4, 6, 10, corde, 1);
        createBlock(4, 8, 12, corde, 1);
        createBlock(1, 8, 18, corde, 1);

        //BOMBES
        addEntite(bombe,1,5);
        addEntite(bombe,3,1);
        addEntite(bombe,4,7);
        addEntite(bombe,9,8);
        addEntite(bombe,17,4);

        //Bot
        createBot(10, 2);
//        createBot(11, 5);
//        addEntite(bot, 10, 2);
//        addEntite(bot, 11, 5);

        //Hector
        addEntite(hector, 1, 7);

        //Radis
        addEntite(radis,3,8);
        addEntite(radis,15,8);


    }

    private void LevelTest()
    {
        //murs extérieurs horizontaux
        createBlock(0, 19, 0, mur, 0);
        createBlock(0, 5, 9, mur, 0);
        createBlock(8, 19, 9, mur, 0);
        createBlock(8, 19, 9, mur, 0);
        for (int x = 8; x <= 19; x++) {
            addEntite(new Mur(this), x, 8);
            addEntite(new Mur(this), x, 8);
        }
        addEntite(new Mur(this), 1, 6);
        addEntite(new Mur(this), 2, 7);
        addEntite(new Mur(this), 3, 8);
        addEntite(new Mur(this), 5, 7);

        //murs extérieurs verticaux
        for (int y = 1; y <= 8; y++) {
            addEntite(new Mur(this), 0, y);
            addEntite(new Mur(this), 19, y);
        }


//        createBlock(1, 3, 4, colonne, 1);
//        createBlock(1, 3, 5, colonne, 1);
//        createBlock(5, 7, 6, colonne, 1);
//        createBlock(4, 6, 7, colonne, 1);
//        createBlock(1, 3, 9, colonne, 1);
//        createBlock(3, 5, 14, colonne, 1);
//        createBlock(3, 5, 15, colonne, 1);


        //Bot
        createBot(10, 7);
//        createBot(11, 5);
//        addEntite(bot, 10, 2);
//        addEntite(bot, 11, 5);

        //Hector
        addEntite(hector, 1, 5);


    }

    private void initialisationDesEntites() {
        mur = new Mur(this);
        corde = new Corde(this);
        hector = new Heros(this);
        bot = new Bot(this);
        bombe = new Bombe(this);
        radis = new Radis(this);

        Gravite g = new Gravite();
        g.addEntiteDynamique(hector); //Appliquer gravité a hector
        ordonnanceur.add(g); //ajoute la gravité au systeme

        Controle4Directions.getInstance().addEntiteDynamique(hector);
        ordonnanceur.add(Controle4Directions.getInstance());


//        Level1();
        LevelTest();
    }


    public void addEntite(Entite e, int x, int y) {
        grilleEntites[x][y] = e;
        map.put(e, new Point(x, y));
    }
    
    /** Permet par exemple a une entité  de percevoir sont environnement proche et de définir sa stratégie de déplacement
     *
     */
    public Entite regarderDansLaDirection(Entite e, Direction d) {
        Point positionEntite = map.get(e);
        return objetALaPosition(calculerPointCible(positionEntite, d));
    }

    /** sert au smics pour savoir si hector est dans leur ligne de vue pour qu'il puisse le poursuivre
     *
     */
    public Direction regarderSurLaLigne(Entite e) {
        Point positonObservateur = map.get(e);
        Point pointeur = new Point(positonObservateur.x, positonObservateur.y);
        //TODO ENLEVER LE INSTANE OF
        pointeur.x ++;
        while (contenuDansGrille(pointeur) &&
                (grilleEntites[pointeur.x][pointeur.y] == null ||
                 grilleEntites[pointeur.x][pointeur.y] instanceof Corde))
        {
            pointeur.x++;
        }
        //TODO ENLEVER LE INSTANE OF
        if (contenuDansGrille(pointeur) && grilleEntites[pointeur.x][pointeur.y] instanceof Heros)
            return Direction.droite;

        pointeur.x = positonObservateur.x -1;
        while (contenuDansGrille(pointeur) &&
                (grilleEntites[pointeur.x][pointeur.y] == null ||
                        grilleEntites[pointeur.x][pointeur.y] instanceof Corde))
        {
            pointeur.x--;
        }
        if (contenuDansGrille(pointeur) && grilleEntites[pointeur.x][pointeur.y] instanceof Heros)
            return Direction.gauche;
        return Direction.haut;//valeur de retour dans le cas ou hector n a pas ete trouvee
    }
    /** Permet par exemple a une entité  de percevoir le terrain en dessous de la case dans la direction donnee
     *
     */
    public Entite regarderTerrainSuivant(Entite e, Direction d) {
        Point positionEntite = map.get(e);
        Point tmp = new Point(positionEntite.x, positionEntite.y);
        tmp.y++;
        return objetALaPosition(calculerPointCible(tmp, d));
    }
    
    /** Si le déplacement de l'entité est autorisé (pas de mur ou autre entité), il est réalisé
     * Sinon, rien n'est fait.
     */
    public boolean deplacerEntite(Entite e, Direction d) {
        boolean retour = false;
        
        Point pCourant = map.get(e);
        
        Point pCible = calculerPointCible(pCourant, d);
        if (contenuDansGrille(pCible) && objetALaPosition(pCible) == null) { // a adapter (collisions murs, etc.)
            //TODO
            // compter le déplacement : 1 deplacement horizontal et vertical max par pas de temps par entité
            switch (d) {
                case bas:
                case haut:
                    if (cmptDeplV.get(e) == null) {
                        cmptDeplV.put(e, 1);

                        retour = true;
                    }
                    break;
                case gauche:
                case droite:
                    if (cmptDeplH.get(e) == null) {
                        cmptDeplH.put(e, 1);
                        retour = true;

                    }
                    break;
            }
        }

        if (retour) {
            deplacerEntite(pCourant, pCible, e);
        }

        return retour;
    }
    
    
    private Point calculerPointCible(Point pCourant, Direction d) {
        Point pCible = null;
        
        switch(d) {
            case haut: pCible = new Point(pCourant.x, pCourant.y - 1); break;
            case bas : pCible = new Point(pCourant.x, pCourant.y + 1); break;
            case gauche : pCible = new Point(pCourant.x - 1, pCourant.y); break;
            case droite : pCible = new Point(pCourant.x + 1, pCourant.y); break;     
            
        }
        
        return pCible;
    }
    
    private void deplacerEntite(Point pCourant, Point pCible, Entite e) {
        grilleEntites[pCourant.x][pCourant.y] = null;
        grilleEntites[pCible.x][pCible.y] = e;
        map.put(e, pCible);
    }
    
    /** Indique si p est contenu dans la grille
     */
    private boolean contenuDansGrille(Point p) {
        return p.x >= 0 && p.x < SIZE_X && p.y >= 0 && p.y < SIZE_Y;
    }
    
    private Entite objetALaPosition(Point p) {
        Entite retour = null;
        
        if (contenuDansGrille(p)) {
            retour = grilleEntites[p.x][p.y];
        }
        
        return retour;
    }
    public int getNbBombes() {
        return nbBombes;
    }
    public int getNbRadis() { return nbRadis; }
    public int getNbVie() {
        return vie;
    }
    public Ordonnanceur getOrdonnanceur() {
        return ordonnanceur;
    }

}
