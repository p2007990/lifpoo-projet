package modele.deplacements;

import modele.plateau.Entite;
import modele.plateau.EntiteDynamique;

public class Bot extends RealisateurDeDeplacement{

    private Direction directionCourante;


    public Bot() {
        this.directionCourante = Direction.gauche;
    }

    private void invertionDirection()
    {
        if (directionCourante == Direction.gauche)
            directionCourante = Direction.droite;
        else
            directionCourante = Direction.gauche;
    }


    private Direction recherchHector(modele.plateau.Bot b)
    {
        return b.regarderSurLaLigne(b);
    }

//TODO AJOUTE LA GRAVITER AU BOTS
    @Override
    //TODO FAIre EN SORTE QUE LE BOT CHANGE DE DIRECTION SI HECTOR EST SUR LA MEME LIGNE
    protected boolean realiserDeplacement() {
        boolean ret = false;
        for (EntiteDynamique e : lstEntitesDynamiques) {
            Direction testHector = recherchHector((modele.plateau.Bot) e);
            if (testHector != Direction.haut)
                directionCourante = testHector;
            Entite eCible = e.regarderDansLaDirection(directionCourante);
            Entite eTerrainCible = e.regarderTerrainDirection(directionCourante);
            if (eCible == null || (eCible != null && !eCible.peutServirDeSupport()))
            {
                //TODO CHECK ELE TYPE DE TERRAIN AVEC GETTYPE ?
                if (eTerrainCible != null && eTerrainCible.peutServirDeSupport())
                {
                    if (e.avancerDirectionChoisie(directionCourante))
                    {
                        ret = true;
                    }
                    else
                    {
                        invertionDirection();
                    }
                }
                else {
                    invertionDirection();
                }
            }
            else
            {
                invertionDirection();
            }
        }

        return ret;
    }
}
