package modele.deplacements;

import modele.plateau.Entite;
import modele.plateau.EntiteDynamique;

/**
 * A la reception d'une commande, toutes les cases (EntitesDynamiques) des colonnes se déplacent dans la direction définie
 * (vérifier "collisions" avec le héros)
 */
public class Colonne extends RealisateurDeDeplacement {
    private Direction directionCourante;
    private static int nbCollones = 0;
    private static Colonne[] colonneListe = new Colonne[99];

    public Colonne() {
        this.directionCourante = Direction.bas;
        colonneListe[nbCollones] = this;
        nbCollones++;
    }

    public static int getNbCollone() {
        return nbCollones;
    }

    public void setDirectionCourante(Direction directionCourante) {
        this.directionCourante = directionCourante;
    }

    public Direction changerDeDIrection()
    {
        if (directionCourante == Direction.bas)
            directionCourante = Direction.haut;
        else
            directionCourante = Direction.bas;
        return directionCourante;
    }
    public static Colonne[] getInstances() {
        return colonneListe;
    }
    protected boolean realiserDeplacement() {//TODO VOIR SI JEU PEUT ENLEVER LE INSTANCEOF
        boolean ret = false;
        for (EntiteDynamique e : lstEntitesDynamiques) {
            Entite eCible = e.regarderDansLaDirection(directionCourante);
            if (eCible == null || (eCible != null && !eCible.peutServirDeSupport()) ||
                    eCible instanceof modele.plateau.Colonne) {
                if (e.avancerDirectionChoisie(directionCourante))
                {
                    ret = true;
                }
            }
        }

        return ret;
    }
}
